#include <stdio.h>
#include <stdlib.h>

typedef struct Matrice {
    int **values;
    int i;
    int j;
} matrix;

int maximum(int *arr, int length) { //array non vide
    int largest = *arr;
    for (int i = 0; i < length; i++) if (*(arr+i) > largest) largest = *(arr+i);
    return largest;
}
void ops(int a, int b, int *s, int *p) {
    *s = a + b;
    *p = a * b;
}
void minmax(int *arr, int length, int *pmin, int *pmax) {
    *pmin = *arr;
    *pmax = *arr;
    for (int i = 0; i < length; i++) {
        if (*(arr+i) < *pmin) *pmin = *(arr+i);
        if (*(arr+i) > *pmax) *pmax = *(arr+i);
    }
}
int* copy(int *arr, int n) {
    int *arr2 = malloc(sizeof(*arr) * n);
    for (int i = 0; i < n; i++) {
        arr2[i] = arr[i];
    }
    return arr2;
}
int* indicesPairs(int* arr, int n) {
    int length2 = ((n % 2 == 0) ? (n/2) : (n/2 + 1));
    int *arr2 = malloc(sizeof(*arr) * length2);
    for (int i = 0; i < n; i++) {
        arr2[i] = arr[i*2];
    }
    return arr2;
}
void matDisplay(matrix mat) {
    for (int a = 0; a < mat.j; a++) {
        for (int b = 0; b < mat.i; b++) {
            printf("%3i", mat.values[a][b]);
        }
        printf("\n");
    }
}
matrix matIntMk(int *vals, int i, int j) {
    int **vls = malloc(i*j*sizeof(int));
    for (int y = 0; y < j; y++) {
        int *arr = malloc(i*sizeof(int));
        for (int x = 0; x < i; x++) arr[x] = vals[x + y*i];
        vls[y] = arr;
    }
    matrix m = { .i = i, .j = j, .values = vls};
    return m;
}
void matErase(matrix mat) {
    for (int y = 0; y < mat.j; y++) {
        free(mat.values[y]);
    }
}
matrix matIntMultiply(matrix m1, matrix m2) {
    //on suppose que les matrices sont compatibles
    int *vals = malloc(m2.i * m1.j * sizeof(int));
    for (int y = 0; y < m1.j; y++) {
        for (int x = 0; x < m2.i; x++) {
            for (int z = 0; z < m1.i; z++) {
                vals[x+y*m2.i] += (m1.values[y][z] * m2.values[z][x]);
            }
        }
    }
    matrix m3 = matIntMk(vals, m2.i, m1.j);
    return m3;
}

void testArrays() {
    int size = 6;
    int ogArr[] = {3, 5, -4, 7, 1, 2};
    int min, max;
    minmax(&ogArr[0], size, &min, &max);
    printf("smallest : %i, largest : %i\n", min, max);
    copy(ogArr, size);
    printf("size : %ld\n", sizeof(ogArr));
    int *newArr = indicesPairs(ogArr, size);
    for (int i = 0; i < ((size % 2 == 0) ? (size/2) : (size/2 + 1)); i++) printf("%i : %i\n", i, newArr[i]);
}
void testMethods() {
    int x, y;
    ops(12, 4, &x, &y);
    printf("x = %d, y = %d\n", x, y);
}
void testMat() {
    int v1[6] = {
        1, 3,
        4, 7,
        2, 0,
    };
    int v2[8] = {
        1, 2, 0, 4,
        1, 0, 3, 1,
    };
    matDisplay(matIntMultiply(matIntMk(v1, 2, 3), matIntMk(v2, 4, 2)));
}

int main() {

    testMat();
}
